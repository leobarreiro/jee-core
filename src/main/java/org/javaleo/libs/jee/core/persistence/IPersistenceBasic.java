package org.javaleo.libs.jee.core.persistence;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Criteria;
import org.javaleo.libs.jee.core.model.IEntityBasic;

@Local
public interface IPersistenceBasic<T extends IEntityBasic> extends Serializable {

	/**
	 * Gets an EntityManager object
	 * 
	 * @return entityManager
	 */
	EntityManager getEntityManager();

	/**
	 * Gets a JPA CriteriaBuilder from EntityManager.
	 * 
	 * @return
	 */
	CriteriaBuilder getCriteriaBuilder();

	/**
	 * Creates a typed JPA CriteriaQuery.
	 * 
	 * @param clazz
	 * @return
	 */
	CriteriaQuery<T> createCriteriaQuery(Class<T> clazz);

	/**
	 * Creates a Hibernate Criteria
	 * 
	 * @param clazz
	 * @param alias
	 * @return
	 */
	Criteria createCriteria(Class<T> clazz, String alias);

	/**
	 * Debugs the resultant query from a CriteriaQuery previously assembled.
	 * 
	 * @param criteriaQuery
	 */
	void logQuery(CriteriaQuery<T> criteriaQuery);

	/**
	 * Gets a List resultant from a CriteriaQuery Select.
	 * 
	 * @param criteriaQuery
	 * @return
	 */
	List<T> getResultList(CriteriaQuery<T> criteriaQuery);

	/**
	 * Gets a List resultant from a CriteriaQuery Select.
	 * 
	 * @param criteriaQuery
	 * @param maxResult
	 * @return
	 */
	List<T> getResultList(CriteriaQuery<T> criteriaQuery, int maxResult);

	/**
	 * Gets a single database object from a CriteriaQuery.
	 * 
	 * @param criteriaQuery
	 * @return
	 */
	T getSingleResult(CriteriaQuery<T> criteriaQuery);

	/**
	 * Gets a Result List from a Hibernate Criteria.
	 * 
	 * @param criteria
	 * @return
	 */
	List<T> getResultList(Criteria criteria);

	/**
	 * Gets a single database object from a Hibernate Criteria.
	 * 
	 * @param criteria
	 * @return
	 */
	T getSingleResult(Criteria criteria);

	/**
	 * Persists an object in database. If the object have an Id, performs a new insert. Otherwise performs an update.
	 * 
	 * @param pojo
	 *            Method Parameter.
	 */
	void saveOrUpdate(T pojo);

	/**
	 * Persists a list of objects in database.
	 * 
	 * @param pojos
	 *            Method Parameter.
	 */
	void saveOrUpdate(List<T> pojos);

	/**
	 * Removes an object from database.
	 * 
	 * @param pojo
	 *            Method Parameter.
	 */
	void remove(T pojo);

	/**
	 * Finds an object whose Id was passed by parameter.
	 * 
	 * @param clazz
	 *            Method Parameter.
	 * @param id
	 *            Method Parameter.
	 * @return Objeto que deriva de IPojo
	 */
	T find(Class<T> clazz, Object id);

	/**
	 * Performs a flush and after cleans the objects managed by EntityManager.
	 */
	void flushAndClear();

}
