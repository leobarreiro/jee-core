package org.javaleo.libs.jee.core.model;

import java.io.Serializable;

public interface IEntityBasic extends Serializable {

	Object getId();

}
