package org.javaleo.libs.jee.core.security;

import java.io.Serializable;

public interface IJavaLeoCredentials extends Serializable {

	String getUuid();

	void setUuid(String uuid);

	String getName();

	void setName(String name);

	boolean isLoggedIn();

	void clear();

}
