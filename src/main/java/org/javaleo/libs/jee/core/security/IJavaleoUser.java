package org.javaleo.libs.jee.core.security;

import java.io.Serializable;

public interface IJavaleoUser extends Serializable {

	String getName();

	String getUsername();

}
