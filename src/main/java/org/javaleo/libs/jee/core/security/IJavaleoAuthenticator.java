package org.javaleo.libs.jee.core.security;

import java.io.Serializable;

import org.javaleo.libs.jee.core.exceptions.JavaleoException;

public interface IJavaleoAuthenticator extends Serializable {

	void authenticate(final String username, final String password) throws JavaleoException;

	void logoff() throws JavaleoException;

	boolean isLoggedIn();

}
