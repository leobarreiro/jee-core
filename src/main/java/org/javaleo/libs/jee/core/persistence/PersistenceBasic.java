package org.javaleo.libs.jee.core.persistence;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.javaleo.libs.jee.core.model.IEntityBasic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@Stateless
public abstract class PersistenceBasic<T extends IEntityBasic> implements IPersistenceBasic<T> {

	private static final long serialVersionUID = -7985410885374189615L;

	private static Logger LOG = LoggerFactory.getLogger(PersistenceBasic.class);

	@Inject
	private EntityManager entityManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javaleo.libs.jee.core.persistence.IPersistenceBasic#getCriteriaBuilder()
	 */
	@Override
	public CriteriaBuilder getCriteriaBuilder() {
		return entityManager.getCriteriaBuilder();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javaleo.libs.jee.core.persistence.IPersistenceBasic#createCriteriaQuery(java.lang.Class)
	 */
	@Override
	public CriteriaQuery<T> createCriteriaQuery(Class<T> clazz) {
		CriteriaBuilder criteriaBuilder = getCriteriaBuilder();
		return criteriaBuilder.createQuery(clazz);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javaleo.libs.jee.core.persistence.IPersistenceBasic#logQuery(javax.persistence.criteria.CriteriaQuery)
	 */
	@Override
	public void logQuery(CriteriaQuery<T> criteriaQuery) {
		TypedQuery<T> typedQuery = entityManager.createQuery(criteriaQuery);
		LOG.info(typedQuery.unwrap(Query.class).getQueryString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.javaleo.libs.jee.core.persistence.IPersistenceBasic#getResultList(javax.persistence.criteria.CriteriaQuery)
	 */
	@Override
	public List<T> getResultList(CriteriaQuery<T> criteriaQuery) {
		criteriaQuery.distinct(true);
		TypedQuery<T> typedQuery = entityManager.createQuery(criteriaQuery);
		return typedQuery.getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javaleo.libs.jee.core.persistence.IPersistenceBasic#getResultList(javax.persistence.criteria.CriteriaQuery, int)
	 */
	@Override
	public List<T> getResultList(CriteriaQuery<T> criteriaQuery, int maxResult) {
		criteriaQuery.distinct(true);
		TypedQuery<T> typedQuery = entityManager.createQuery(criteriaQuery);
		typedQuery.setMaxResults(maxResult);
		return typedQuery.getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.javaleo.libs.jee.core.persistence.IPersistenceBasic#getSingleResult(javax.persistence.criteria.CriteriaQuery)
	 */
	@Override
	public T getSingleResult(CriteriaQuery<T> criteriaQuery) {
		criteriaQuery.distinct(true);
		TypedQuery<T> typedQuery = entityManager.createQuery(criteriaQuery);
		typedQuery.setMaxResults(1);
		List<T> results = typedQuery.getResultList();
		if (results != null && !results.isEmpty()) {
			return results.get(0);
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javaleo.libs.jee.core.persistence.IPersistenceBasic#createCriteria(java.lang.Class)
	 */
	@Override
	public Criteria createCriteria(Class<T> clazz, String alias) {
		Session session = (Session) getEntityManager().getDelegate();
		Criteria criteria = session.createCriteria(clazz, alias);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return criteria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javaleo.libs.jee.core.persistence.IPersistenceBasic#getResultList(org.hibernate.Criteria)
	 */
	@Override
	public List<T> getResultList(Criteria criteria) {
		@SuppressWarnings("unchecked")
		List<T> resultList = criteria.list();
		return resultList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javaleo.libs.jee.core.persistence.IPersistenceBasic#getSingleResult(org.hibernate.Criteria)
	 */
	@Override
	public T getSingleResult(Criteria criteria) {
		@SuppressWarnings("unchecked")
		List<T> resultList = criteria.list();
		if (resultList != null && !resultList.isEmpty()) {
			return resultList.get(0);
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.javaleo.libs.jee.core.persistence.IPersistenceBasic#saveOrUpdate(org.javaleo.libs.jee.core.model.IEntityBasic
	 * )
	 */
	@Override
	public void saveOrUpdate(T pojo) {
		if (pojo.getId() != null) {
			entityManager.merge(pojo);
		} else {
			entityManager.persist(pojo);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javaleo.libs.jee.core.persistence.IPersistenceBasic#saveOrUpdate(java.util.List)
	 */
	@Override
	public void saveOrUpdate(List<T> pojos) {
		for (T pojo : pojos) {
			saveOrUpdate(pojo);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javaleo.libs.jee.core.persistence.IPersistenceBasic#remove(org.javaleo.libs.jee.core.model.IEntityBasic)
	 */
	@Override
	public void remove(T pojo) {
		entityManager.remove(pojo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javaleo.libs.jee.core.persistence.IPersistenceBasic#find(java.lang.Class, java.lang.Object)
	 */
	@Override
	public T find(Class<T> clazz, Object id) {
		T pojo = entityManager.find(clazz, id);
		if (pojo == null) {
			return null;
		} else {
			return pojo;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javaleo.libs.jee.core.persistence.IPersistenceBasic#flushAndClear()
	 */
	@Override
	public void flushAndClear() {
		entityManager.flush();
		entityManager.clear();
	}

}
