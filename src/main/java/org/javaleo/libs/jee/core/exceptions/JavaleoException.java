package org.javaleo.libs.jee.core.exceptions;

public class JavaleoException extends Exception {

	private static final long serialVersionUID = 6772740815894980600L;

	public JavaleoException() {
		super();
	}

	public JavaleoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public JavaleoException(String message, Throwable cause) {
		super(message, cause);
	}

	public JavaleoException(String message) {
		super(message);
	}

	public JavaleoException(Throwable cause) {
		super(cause);
	}

}
