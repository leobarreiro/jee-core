package org.javaleo.libs.jee.core.web.actions;

import java.io.Serializable;

import javax.enterprise.context.Conversation;

public abstract class AbstractCrudAction implements Serializable {

	private static final long serialVersionUID = 1L;

	public abstract Conversation getConversation();

	public void startNewConversation() {
		endConversation();
		getConversation().begin();
	}

	public void startOrResumeConversation() {
		if (getConversation().isTransient()) {
			getConversation().begin();
		}
	}

	public void endConversation() {
		if (!getConversation().isTransient()) {
			getConversation().end();
		}
	}

}
