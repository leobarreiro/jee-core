package org.javaleo.libs.jee.core.security;

public abstract class JavaLeoCredentials implements IJavaLeoCredentials {

	private static final long serialVersionUID = -7630051656876718564L;

	private String name;
	private String uuid;

	public void clear() {
		this.name = null;
		this.uuid = null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public boolean isLoggedIn() {
		return (name != null && name.length() > 0 && uuid != null && uuid.length() > 0);
	}

}
